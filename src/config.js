import axios from "axios";
const baseApiUrl =  process.env.NODE_ENV === "development" ? "http://localhost:8080" : window.location.origin
const remoteUrl =  process.env.NODE_ENV === "development" ? "https://stone.lizhengtech.com":"https://stone.lizhengtech.com"

const fecthApi = (path,method,data)=>{
       return  Promise.race([
        axios({
          url: baseApiUrl+path,
          method: method,
          headers: {
            'Content-Type': 'application/json',
            // 'Authorization': `Bearer ${localStorage.getItem('id_token')}`,
          },
          data: JSON.stringify(data)
        }),
        new Promise(function (resolve, reject) {
          setTimeout(() => reject(new Error('request timeout')), 30000)
        })
      ]).then(res => {
        if (res.status === 401) {
          return Promise.reject({ error: res.statusText });
        }
        return res.data
      }).then(data => {
        if (data.code != "1") {
          return Promise.reject(new Error(data.message))
        }
        return data;
      })
        .then(
          data => ({ data }),
          error => ({ error: error.message })
        );
}

const fecthRemoteApi = (path,method,data)=>{
  return  Promise.race([
   axios({
     url: remoteUrl+path,
     method: method,
     headers: {
       'Content-Type': 'application/json',
       // 'Authorization': `Bearer ${localStorage.getItem('id_token')}`,
     },
     data:  JSON.stringify(data)
   }),
   new Promise(function (resolve, reject) {
     setTimeout(() => reject(new Error('request timeout')), 30000)
   })
 ]).then(res => {
   if (res.status === 401) {
     return Promise.reject({ error: res.statusText });
   }
   return res.data
 }).then(data => {
   if (data.code != 1) {
     return Promise.reject(new Error(data.message))
   }
   return data;
 })
   .then(
     data => ({ data }),
     error => ({ error: error.message })
   ).catch(err=>{
    console.log("catch err",err)
   })
}

export{
    fecthApi,
    fecthRemoteApi    
}


