
import styled from 'styled-components'

const TopbarStyle=styled.div`
    .topbar{
        position: absolute;
        z-index: 999;
        left: 5%;
        top: 1%;
        display: flex;
        width: 15vw;
        justify-content: center;
        align-items: center;
        height: 5vh;
        background-color: white;
        border-radius: 10px;
    }

    #topbar_img{
        right: -2%;
        position: relative;
        height: 20px;
        &:hover{
            content: url('/images/act_position.svg')
        }
    }

    .right_bar{
        position: absolute;
        z-index: 999;
        width: 15vw;
        height: 5vh;
        display: flex;
        justify-content: center;
        right: 5%;
        top: 1%;
        background-color: white;
        border-radius: 10px;
        opacity: 50%;
    }

    .user_img{
        position: relative;
        height: 30px;
        display: inline-block;
        justify-content: center;
        margin-top: 4px;
        margin-right: 10px;
        margin:auto
        &:hover{
            content: url('/images/user_act.svg')
        }
    }
    .task_img{
        height: 28px;
        display: inline-block;
        margin:auto
    }
    .conn{
        width: 30%;
        height: 75%;
        margin-top: 5px;
    }
`
 
export { TopbarStyle }