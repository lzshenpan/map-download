import React, { useEffect, useState, useRef } from 'react';
import { Navigate } from 'react-router-dom';
import { fecthRemoteApi } from '../config';
import { saveInfo } from '../utils/help';
import { message } from 'antd';
import { SignStyle } from '../style/sign';

export default function Sign() {
    const [userInfo, setUserInfo] = useState({})
    const [isLogin, setLogin] = useState(false)
    const [codeList, setCode] = useState("")
    const inputValue = useRef("");
    const handleChange = (e) => {
        const { name, value } = e.target
        let data = Object.assign({ ...userInfo }, { [name]: value })
        setUserInfo(data)
    }

    const Login = async (props) => {
        if (!codeList || codeList.length == 0) {
            refreshCode()
        }
        // verify code 
        await Codelist("", inputValue.current.value)
        try{
            fecthRemoteApi('/v1/map/login', 'post', userInfo).then(response => {
                const { data, error } = response
                console.log("response",response, data, error)
                if (data) {
                    const { code, data: res } = data
                    if (code == 1) {
                        //login successful
                        saveInfo(res)
                        localStorage.setItem("isLogin",true)
                        setLogin(true)
                    }
                } else {
                    console.log("error login reason :", error)
                    message.error(`${error}`,3)
                    refreshCode()
                }
            }).catch(err=>{
                console.log("login err",err)
            })

        }catch(err){
            console.log(err)
        }
    }

    // 验证码
    const VerificationCode = () => {
        const codeList = []
        // const chars = 'abcdefhijkmnprstwxyz0123456789'
        const chars = '0123456789'
        const charsLen = chars.length // 数字-10 、 字母+数字-30

        // 生成随机数
        for (let i = 0; i < 4; i++) {
            const rgb = [Math.round(Math.random() * 220), Math.round(Math.random() * 240), Math.round(Math.random() * 200)]
            codeList.push({
                code: chars.charAt(Math.floor(Math.random() * charsLen)),
                color: `rgb(${rgb})`,
                // fontSize: '20px',
                // padding: '6px'
            })
        }
        console.log('4个随机数', codeList)
        setCode([...codeList])


    }

    const refreshCode = () => {
        VerificationCode()
        if (inputValue && inputValue.current){ inputValue.current.value = ""}

    }
    // 验证码校验
    const Codelist = (rule, value) => {
        return new Promise(
            (resolve, reject) => {
                console.log('验证码校验', value);
                if (codeList.length!==0 && value === codeList[0].code + codeList[1].code + codeList[2].code + codeList[3].code) {
                    resolve("true")
                } else {
                    message.success('验证码输入有误, 请重新输入', 3);
                    refreshCode()
                    reject("false")
                }
            }
        )
    }

    return (
        <SignStyle>
            {isLogin && <Navigate to="/dashboard" replace="true"></Navigate>}
            <div className='bj'>
                <div className="header">
                    <div className="header-center">
                        <div className="header-title">
                            <h1>地图下载器</h1>
                            <h2>Map Downloader</h2>
                        </div>
                        <div className="header-img"></div>
                    </div>
                    <div className="header-bottom"></div>

                </div>

                <div className="content">
                    <div className="content-left">
                        {/* <img src="/images/d.png" alt="" /> */}
                    </div>
                    <div className="content-right">
                        <div className="right-infp">
                            <div className="right-infp-name">
                                <input type="text" name="username" placeholder="请输入用户名" required="" onChange={handleChange} autoComplete="off" />
                            </div>
                            <div className="right-infp-name">
                                <input type='password' name='password' placeholder="请输入密码" autoComplete="off" onChange={handleChange} />
                            </div>
                            {codeList && codeList.length != 0 && 
                                <div>
                                    <div className="verifycode">
                                        {codeList.map((item, index) => {
                                            return (
                                                <span key={index} style={{ color: item.color, fontSize: '20px', padding: '0 6px' }}>{item.code}</span>
                                            )
                                        })}
                                        <span onClick={refreshCode} style={{textDecoration:"underline",cursor:"pointer"}}>刷新</span>
                                    </div>
                                    <div className="right-infp-name">
                                        <input ref={inputValue} type='text' placeholder="请输入验证码" autoComplete="off" />
                                    </div>
                                </div>
                            }

                            <div className="right-infp-btn">
                                <button className="btn" onClick={Login}>登录</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </SignStyle>
    )
}
