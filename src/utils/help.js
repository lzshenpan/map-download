import { fecthApi } from '../config'

function saveInfo(data) {
    Object.keys(data).forEach((key) => {
        if (key !== "history") {
            localStorage.setItem(key.trim(), data[key])
        }
    })
}

function removeInfo() {
    localStorage.clear()
}

const downloadStateQuery = (dispatch) => {
    fecthApi("/v1/state", "Get").then(res => {
        if (res.data) {
            const { data } = res.data
            let newData = {
                percent: data.percent,
                tileDone: data.tiledone,
                tileErr: data.tileErr,
                finish: data.complete,
                downloading: data.downloading
            }
            dispatch({ type: "success", payload: newData })
        }
    })
}

export {
    saveInfo,
    removeInfo,
    downloadStateQuery
}