import React, { useEffect, useState, useReducer} from 'react';
import { useNavigate } from 'react-router-dom';
import { DownloadWrapper } from '../reducers/input';
import { TopbarStyle } from '../style/topbar';
import Map from './dashboard';
import Input from './topbar';
import Userbar from './userbar';

export default function Dashboard() {

    const [point, setUpdate] = useState(null)
    const [ignored, forceUpdate] = useReducer(x => x + 1, 0);
    const navigate = useNavigate()
    const setPoint = (point)=>{
        setUpdate(point)
        forceUpdate()
    }

    useEffect(() => {
        if(!localStorage.getItem("isLogin")){
            navigate("/sign",{replace:true})
        }
        document.checkRegion()
    },[])


    return (
        <div style={{ width: '100vw', height: '100vh' }}>
            <DownloadWrapper>
                <Map point={point} />
                <TopbarStyle>
                    <Input setPoint={(point)=>{setPoint(point)}}  ></Input>
                    <Userbar />
                </TopbarStyle>
            </DownloadWrapper>

        </div>
    );

}

