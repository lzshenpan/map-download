import { message, Modal, Collapse, Switch } from 'antd';
import React, { useState, useImperativeHandle, useContext } from 'react';
import { useEffect } from 'react';
import { fecthApi, fecthRemoteApi } from '../config'
import { DownloadContext } from '../reducers/input';
import { Button, Progress } from 'antd';
import './style.css'


function bytesToSize(bytes) {
    if (bytes === 0) return '0 B';

    var k = 1024, // or 1024

        sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],

        i = Math.floor(Math.log(bytes) / Math.log(k));

    return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];

}

const ModalInfo = (props, ref) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [mapInfo, setMapInfo] = useState({
        'maxLng': "",
        'minLng': "",
        'minLat': "",
        'maxLat': "",
        'minZ': 0,
        'maxZ': 0,
        'type': "",
        'lang': "",
    })
    const [gudgeSize, setGudgeSize] = useState(null)
    const [totalCount, setTotal] = useState(null)
    const [show, setShow] = useState(false)
    const [load, setLoad] = useState(false)
    const {dispatch, connState,showDlPanel,showDispatch} = useContext(DownloadContext)
    const showModal = () => {
        setIsModalVisible(true);
    };
    useImperativeHandle(ref, () => ({
        showModal: showModal,
    }));

    const preDownload = async () => {
        //check if have downloading task
        const p1 = new Promise((resolve, reject) => {
            fecthApi("/v1/state", "Get").then((res) => {
                console.log("check state info")
                const { data } = res.data
                if (data.downloading) {
                    message.info("您有正在下载的任务，请等待下载任务结束再继续")
                    reject(false)
                }
                resolve(true)
            })
        })

        const p2 = new Promise((resolve, reject) => {
            fecthRemoteApi("/v1/map/query/" + `${localStorage.getItem("userId")}`, "Get").then(res => {
                // console.log("check user info", res)
                const { data: newdata, error } = res
                if (error) {
                    message.info(error)
                    reject(false)
                }
                const { data } = newdata
                if (data.authority && (data.role === "root" || !data.is_expire || data.account > 0)) {
                    resolve(true)
                } else {
                    message.info("未授权或者授权免费时间已过期，请联系授权或者充值使用")
                    reject(false)
                }
            })
        })
        let res = await Promise.all([p1, p2]).catch(err => { return err })
        // let checkUser = localStorage.getItem("authority") && (localStorage.getItem("role") === "root" || data.now < localStorage.getItem("expire_time") || localStorage.get("account") > 0)
        return res
    }

    const startDownload = async () => {
        let checkOk = await preDownload()
        if (checkOk) {
            fecthApi(`/v1/start/${localStorage.getItem("userId")}`, "Get").then(response => {
                const { code } = response.data
                if (code == 1) {
                    //download successful
                    // console.log("succeessfull start download")
                    setIsModalVisible(false)
                    // dispatch({type:"success",payload:{downloading: true}})
                    // setShow(true)
                    showDispatch({type:"show"})
                }
            })
        }
    }

    const handleOk = () => {
        //check if connet to backend
        console.log("conn state", connState)
        if (!connState) {
            message.info("后台连接关闭，请检查后台连接")
            return
        }
        //check input valid
        let checkOk = Object.keys(mapInfo).every(key => {
            if (!mapInfo[key]) {
                message.info("输入信息不能为空")
                return false
            } else {
                return true
            }
        })
        // setIsModalVisible(false);
        if (!checkOk) { return }
        if (totalCount) {
            startDownload()
        } else {
            setLoad(true)
            mapInfo["maxZ"] = parseInt(mapInfo.maxZ)
            mapInfo["minZ"] = parseInt(mapInfo.minZ)
            let base64 = btoa(JSON.stringify(mapInfo))
            fecthApi(`/v1/mapinfo/${base64}`, "Get").then(response => {
                const { data } = response.data
                let gudge = bytesToSize(data * 1024 * 15)
                setTotal(data, [totalCount])
                setGudgeSize(gudge, [gudgeSize])
                setLoad(false)
                console.log("map config info", mapInfo)
            })
        };
    }


    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleChange = (e) => {
        if (!mapInfo.type || !mapInfo.lang) {

        }
        const { name, value } = e.target
        let data = Object.assign({ ...mapInfo }, { [name]: value })
        setMapInfo(data)
    }
    const checkZoom = (e)=>{
        if (e.target.value<3){
            e.target.value=3
        }
        if(e.target.value>18){
            e.target.value=18
        }
    }

    const reset = () => {
        if (!isModalVisible) {
            setGudgeSize(null)
            setTotal(null)
            setLoad(false)
        }

    }

    useEffect(() => {
        let data = Object.assign(mapInfo, props.range)
        setMapInfo(data)
        return () => {
            reset()
        }
    })

    return (
        <div className='modal'>
            {/* <DownLoadPanel></DownLoadPanel> */}
            <Modal title="输入下载地图信息" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} style={{ position: "absolute", zIndex: "999", left: "35%" }}
                okText={totalCount ? "下载" : load ? "loading" : "完成"}
            >
                <div className='item'><span>{"最小层级"}:  </span><input name='minZ' onChange={handleChange} type="number" max={18} min={3} onBlur={e=>{checkZoom(e)}} autoComplete="off"></input>  </div>
                <div className='item'><span>{"最大层级"}:  </span><input name='maxZ' onChange={handleChange} type="number" min={3} max={18} onBlur={e=>{checkZoom(e)}} autoComplete="off"></input>  </div>
                <div className='item'><span>{"地图类型"}:  </span><select name='type' onClick={handleChange} >
                    <option value="">--请选择--</option>
                    <option value={0} disabled={localStorage.getItem("role") === "root" || localStorage.getItem("region") !== "cn" ? false : true}>谷歌矢量(深色)</option>
                    <option value={4} disabled={localStorage.getItem("role") === "root" || localStorage.getItem("region") !== "cn" ? false : true}>谷歌矢量(浅色)</option>
                    <option value={1} disabled={localStorage.getItem("role") === "root" || localStorage.getItem("region") !== "cn" ? false : true}>谷歌卫星</option>
                    <option value={2} disabled={localStorage.getItem("role") === "root" || localStorage.getItem("region") !== "cn" ? false : true}>谷歌(矢量+卫星)</option>
                    <option value={3} disabled={localStorage.getItem("role") === "root" || localStorage.getItem("region") === "cn" ? false : true}>高德地图</option>
                </select>
                </div>
                <div className='item'><span>{"地图语言"}:  </span><select name='lang' onClick={handleChange}>
                    <option value="">--请选择--</option>
                    <option value="zh">中文(zh)</option>
                    <option value="en" disabled={mapInfo.type=="3" ? true : false}>英文(en)</option>
                </select>
                </div>
                <div>
                    <ConfigPanel></ConfigPanel>
                </div>
                <hr></hr>
                {totalCount && <ConfirmInfo totalCount={totalCount} gudgeSize={gudgeSize}></ConfirmInfo>}
            </Modal>
            {showDlPanel && <DownLoadPanel show={showDlPanel} setshow={showDispatch}></DownLoadPanel>}
        </div>
    );
};

const ConfirmInfo = (props) => {
    return (
        <div>
            {/* <p>{"高级选项"}</p> */}
            <p style={{ position: "relative", display: "flex", textAlign: "center" }}>{"下载信息确认"}</p>

            <p>下载瓦片数量：{props.totalCount}<br /></p>
            <p>预计文件大小：{props.gudgeSize}<br /></p>
                
        </div>
    )
}

const DownLoadPanel = (props) => {
    const [state, setState] = useState({
        percent: 0,
        tileDone: 0,
        tileErr: 0,
        finish: null
    })

    const [showPanel, setShow] = useState(true)

    const {dispatch } = useContext(DownloadContext)

    const stateQuery = () => {
        fecthApi("/v1/state", "Get").then((res) => {
            const { data } = res.data
            let newData = {
                percent: data.percent,
                tileDone: data.tiledone,
                tileErr: data.tileErr,
                finish: data.complete,
                downloading: data.downloading
            }
            let result = Object.assign({ ...state }, newData)
            dispatch({type:"success",payload:newData})
            setState(result)
        })
    }


    useEffect(() => {
        const tick = setInterval(stateQuery, 1000);
        return () => {
            clearInterval(tick)
        }
    }, [])

    useEffect(() => {
        props.show && setShow(true)
        return () => {
            if (!showPanel) {
                // props.setshow(false)
                props.setshow({type:"noshow"})
            }
        }
    })

    return (
        <Modal title="下载进度" visible={showPanel} onOk={() => { setShow(false) }} onCancel={() => { setShow(false) }} style={{ position: "absolute", zIndex: "999", left: "35%" }}
            okButtonProps={{
                disabled: state.finish ? false : true
            }}
            cancelButtonProps={{
                disabled:true,
                // display: false
            }}
        >
            <Progress percent={Math.floor(state.percent * 100)} />
            <p>
                下载成功数量：{state.tileDone}<br />
                下载失败数量：{state.tileErr}<br />
            </p>
        </Modal>
    )
}

const { Panel } = Collapse
const ConfigPanel = () => {
    const [edit, setEdit] = useState(false)
    const [cfgCurrent, setCfg] = useState({
        "div": 1,
        "add": "false",
        "work": 128
    })
    const [cfgTemp, setCfgTemp] = useState({})
    const handleClick = (e) => {
        const { name } = e.target
        //取消操作
        if (name == "cancel") {
            e.stopPropagation()
            setEdit(false)
            return
        }

        //确认操作
        if (name == "ok") {
            e.stopPropagation()
            //check input valid
            let checkOk = true
            let body = {}
            let needUpdate = false
            Object.keys(cfgTemp).map(key => {
                needUpdate = true
                if (key === "div" && !cfgTemp[key]) {
                    message.info("请输入分表数量1~20范围,")
                    checkOk = false
                    return
                } else {
                    if (key == "div" || key == "work") {
                        let val = parseInt(cfgTemp[key])
                        body[key] = val
                    } else {
                        body[key] = cfgTemp[key]
                    }
                }
            })
            if (!needUpdate) {
                setCfgTemp({})
                setEdit(false)
                return
            }
            //send modify api 
            checkOk && fecthApi("/v1/optional", "post", body).then(res => {
                const { data, error } = res
                if (error) {
                    message.error(`更改配置项失败:${error}`)
                } else {
                    message.success("更改配置项成功")
                    const { div, work, add } = data.data
                    let newData = { div, work, add }
                    setCfg(Object.assign({ ...cfgCurrent }, newData))
                    setCfgTemp({})
                    setEdit(false)
                }
            })

            // close
        }
        if (name == "cfg") {
            e.stopPropagation()
            setEdit(true)
        }

    }

    const onChange = (e, check) => {
        if (check !== undefined) {
            setCfgTemp(Object.assign({ ...cfgTemp }, { "add": check }))
            return
        }
        const { name, value } = e.target
        setCfgTemp(Object.assign({ ...cfgTemp }, { [name]: value }))
    }

    const getConfig = () => {
        return (
            <>
                {!edit && <img src='/images/config.svg' name="cfg" onClick={(e) => { handleClick(e) }}></img>}
                {edit &&
                    <div>
                        <img src='/images/cancel.svg' name="cancel" onClick={(e) => { handleClick(e) }}></img>
                        &nbsp;&nbsp;
                        <img src='/images/ok.svg' name="ok" onClick={(e) => { handleClick(e) }}></img>
                    </div>

                }
            </>
        )
    }

    return (
        <Collapse >
            <Panel header="高级选项" key="1" edit={edit} handleClick={(e) => { handleClick(e) }} extra={getConfig()}>
                {edit &&
                    <div>
                        <p>分表数量:<input defaultValue={cfgCurrent.div} max={20} min={1} type="number" name="div" onChange={e => onChange(e)} input /></p>
                        <p>地图添加:<Switch name="add" defaultChecked={cfgCurrent.add} onChange={(check, e) => onChange(e, check)}></Switch></p>
                        <div>最大线程:
                            <select name="work" defaultValue={cfgCurrent.work} onChange={e => onChange(e)}>
                                <option value={128} >128</option>
                                <option value={256}>256</option>
                                <option value={512}>512</option>
                            </select>
                        </div>
                    </div>}
                {!edit &&
                    <div>
                        <p>分表数量:{cfgCurrent["div"]}</p>
                        <p>地图添加:{cfgCurrent["add"] ? "开启" : "关闭"}</p>
                        <p>最大线程:{cfgCurrent["work"]}</p>
                    </div>}

            </Panel>
        </Collapse>
    )
}


const MyModal = React.forwardRef(ModalInfo);
export default MyModal
