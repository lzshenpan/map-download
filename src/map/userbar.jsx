import React, { useState, useRef, useEffect, useContext } from 'react'
import { Popover, Drawer, Dropdown, Menu, Table, Modal, message } from 'antd';
import { fecthApi, fecthRemoteApi } from '../config';
import { DownloadContext } from '../reducers/input';
import { downloadStateQuery, removeInfo } from '../utils/help';
import { useNavigate } from 'react-router-dom';
import './style.css'



export default function Userbar(props) {
    const [showModal, setShowModal] = useState(false)
    const [showDrawer, setShow] = useState(false)
    const [showRegion, setShowRegion] = useState(false)
    const [conn, setConn] = useState(true)
    const [history, setHistory] = useState([])
    const navigate = useNavigate()
    const [tasking, setTask] = useState({
        percent: 0,
        tileDone: 0,
        tileErr: 0,
        finish: false,
        downloading: false
    })

    const [queryStart, setStart] = useState(false)

    const { downLoadState, dispatch, connState, connDispatch, showDlPanel, showDispatch } = useContext(DownloadContext)

    const { downloading } = downLoadState

    const menu = (
        <Menu
            onClick={({ key }) => { handleClick(key) }}
            theme='white'
            items={[
                {
                    label: '个人中心',
                    key: '1',
                },
                {
                    label: '修改密码',
                    key: '2',
                },
                {
                    label: 'logout',
                    key: '3',
                },
            ]}
        />
    );

    const content = (
        <div className='taskContent'>
            <p>{conn ? "已连接" : "连接已断开"}</p>
            {downloading && <p onClick={() => { showDispatch({ type: "show" }) }}>任务正在下载(点此查看详情)</p>}
            {!downloading && <p>空闲</p>}

        </div>
    );


    const handleClick = (key) => {
        switch (key) {
            case "1":
                setShow(true)
                break
            case "2":
                setShowModal(true)
                break
            case "3":
                handleLogOut()
                break
        }
    }

    const handleLogOut = () => {
        removeInfo()
        navigate("/sign", { replace: true })
    }

    const onClose = () => {
        setShow(false);
    };


    const History = () => {
        const [dataSource,setData] = useState([])
        const [page, setPage] = useState(1)

        const columns = [
            {
                title: '日期',
                dataIndex: 'date',
                key: 'date',
            },
            {
                title: '大小(MB)',
                dataIndex: 'consume',
                key: 'consume',
            },
            {
                title: '记录',
                dataIndex: 'recording',
                key: 'record',
            },
        ];

        useEffect(()=>{
            fecthRemoteApi("/v1/map/query/" + `${localStorage.getItem("userId")}`, "Get").then(res => {
                const { data: newdata} = res.data
                if (newdata && newdata.history ){
                    const { time, record, size } =  getHistory(newdata.history)
                    const newData = time.map((val, ind) => {
                        let key = ind
                        let date = val
                        let consume = size[ind]
                        let recording = record[ind]
                        return { key, date, consume, recording }
                    })
                    setData(Object.assign([...dataSource],[...newData]))   
                } 
            })
        },[])

        const paginationProps = {
            current: page, //当前页码
            pageSize: 2, // 每页数据条数
            // showTotal: () => (
            //   <span>总共{total}项</span>
            // ),
            total: dataSource.length, // 总条数
            onChange: page => setPage(page), //改变页码的函数
            hideOnSinglePage: false,
            showSizeChanger: false,
        }
        return (
            <>
               {dataSource && <Table columns={columns} dataSource={dataSource} pagination={paginationProps} ></Table>}         
            </>

        )

    }

    const InfoModal = () => {
        const [auth, setAuth] = useState(false)
        const [newPasswd, setNewPasswd] = useState("")
        const [userInfo, setUserInfo] = useState({
            "rawPwd": "",
            "newPwd1": "",
            "newPwd2": "",
        })

        const handleChange = (e) => {
            const { name, value } = e.target
            let newData = Object.assign({ ...userInfo }, { [name]: value })
            setUserInfo(newData)
        }
        const handleOk = () => {
            if (!auth) {
                let data = {
                    "username": localStorage.getItem("username"),
                    "password": userInfo.rawPwd
                }
                fecthRemoteApi('/v1/map/login', 'post', data).then(response => {
                    const { data, error } = response
                    console.log("response", response, data, error)
                    if (data) {
                        const { code, data: res } = data
                        if (code == 1) {
                            //login successful
                            setAuth(true)
                        }
                    } else {
                        message.error(`请输入正确的密码`)
                    }
                }).catch(err => {
                    console.log("login err", err)
                })
            }
            if (auth) {
                //check corret passwd
                if (userInfo.newPwd1 !== userInfo.newPwd2) {
                    message.error("两次密码输入不一致")
                    return
                }
                let data = {
                    "uuid": localStorage.getItem("userId"),
                    "password": userInfo.newPwd1
                }
                fecthRemoteApi('/v1/map/update', 'post', data).then(res => {
                    const { data, error } = res
                    if (data && data.code == 1) {
                        message.success("密码修改成功")
                        setShowModal(false)
                    } else {
                        if (error) { message.error("密码修改失败,", error) }
                        setNewPasswd("")
                    }
                }).catch(err => {
                    console.log("login err", err)
                })
            }
        }

        useEffect(() => {
            return ()=>{
                setAuth(false)
                setUserInfo({})
            }
        }, [])

        return (
            <Modal title="修改密码" visible={showModal} onOk={() => { handleOk() }} onCancel={() => { setShowModal(false) }} style={{ position: "absolute", zIndex: "999", left: "35%" }}
                okButtonProps={{
                    disabled: false
                }}
                cancelButtonProps={{
                    disabled: false,
                    // display: false
                }}
            >
                <div>
                    <p>请输入旧密码：<input type={"password"} name="rawPwd" onChange={e => { handleChange(e) }} /></p>
                    {auth && <p>请输入新密码：<input type={"password"} name="newPwd1" onChange={e => { handleChange(e) }} /></p>}
                    {auth && <p>请输入新密码：<input type={"password"} name="newPwd2" onChange={e => { handleChange(e) }} /></p>}
                </div>
            </Modal>
        )

    }


    useEffect(() => {
        setTimeout(() => {
            setShowRegion(localStorage.getItem("region"))
        }, 3000);
        const tick = setInterval(() => {
            fecthApi("/ping").then(res => {
                const { error } = res
                if (error) {
                    setConn(false)
                    connDispatch({ type: "no" })
                } else {
                    setConn(true)
                    connDispatch({ type: "ok" })
                }
            }).catch(err => { console.log("Errrrrr", err) })
        }, 2000);
        //downloading task query
        downloadStateQuery(dispatch)
        return () => {
            setShowRegion(false)
            clearInterval(tick)
        }
    }, [])


    return (
        <div className='right_bar'>
            {/* {logOut && <Navigate to="/sign"></Navigate>} */}
            <img className='region' src='/images/region.svg' alt='region' />
            {showRegion && <span>{localStorage.getItem("region") && localStorage.getItem("region") == "cn" ? "CH" : "EN"}</span>}
            <img className='conn' src={conn ? '/images/connect.svg' : '/images/123.svg'} alt='region' />
            <Popover content={content} title="下载器状态">
                <img className='task_img' src='/images/task_act.svg' alt='task' />
            </Popover>
            <Dropdown overlay={menu}>
                <img className='user_img' src='/images/user.svg' alt='user' />
            </Dropdown>
            {showModal && <InfoModal />}
            <Drawer
                title="个人中心"
                placement="right"
                onClose={onClose}
                visible={showDrawer}
                closeIcon={<img src='/images/user.svg'></img>}
            >
                <div>
                    <p>
                        <span>
                            用户名:{localStorage.getItem("username")}
                        </span>
                    </p>
                    <hr></hr>
                    <p>用户权限: {localStorage.getItem("role")}</p>
                    <hr></hr>
                    <p>账户余额: {localStorage.getItem("account")}</p>
                    <hr></hr>
                    <p>授权: {localStorage.getItem("authority")}</p>
                    <hr></hr>
                    <p>创建时间: {localStorage.getItem("created_time")}</p>
                    <hr></hr>
                    <p>到期时间: {localStorage.getItem("expire_time")}</p>
                    <hr></hr>
                    <div>
                        下载记录:
                    </div>
                    {showDrawer && <History></History>}
                </div>
            </Drawer>

        </div>
    )
}

//process history
const getHistory = (data) => {
    let decoded = JSON.parse(atob(data));
    let time = []
    let record = []
    let size = []
    Object.keys(decoded).map(key => {
        let start = decoded[key].indexOf("\_")
        time.push(key)
        record.push(decoded[key].substring(start + 1, decoded[key].length))
        size.push(decoded[key].split("_", 1))
    })
    return { time, record, size }
}



