import React, { useState } from "react";
import { useEffect } from "react";


export default function Input(props){
    const [inputValue,setValue] = useState("")
    const handleClick = ()=>{
        props.setPoint(inputValue)
    }
    useEffect(()=>{
        
    })
    return(
        <div className="topbar" >
            <input  style={{width:'80%'}} onChange={(e)=>setValue(e.target.value)} placeholder="lat,lng"></input>
            <img id="topbar_img" src="/images/position.svg" alt={"#"} onClick={handleClick}></img>
        </div>
    )

}