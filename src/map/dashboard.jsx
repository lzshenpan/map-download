import React, { Component } from 'react';
import L from 'leaflet';
import "./leaflet/dist/leaflet.css";
import 'leaflet.pm';
import 'leaflet.pm/dist/leaflet.pm.css';
import MyModal from './modal';
import { ModalStyle } from '../style/Modal';

class Map extends Component {
    constructor() {
        super()
        this.state = {
            showModal: false
        }
        this.map = null;
        this.layer = null;
        this.layerGeojson = null;
        this.range = {}
        this.ChildRef = React.createRef()

    }

    onListenHandle = (e) => {
        this.firstPoint = e.latlng
    }


    caclDistance = (e) => {
        if (this.firstPoint){
            // 定义两个坐标
            var latlng1 = this.firstPoint;
            var latlng2 = e.latlng;

            // 计算水平和垂直距离
            var horizontalDistance = latlng1.distanceTo(L.latLng(latlng1.lat, latlng2.lng));
            var verticalDistance = latlng1.distanceTo(L.latLng(latlng2.lat, latlng1.lng));
            console.log(document.getElementsByClassName("leaflet-tooltip leaflet-zoom-animated leaflet-tooltip-bottom"))
            document.getElementsByClassName("leaflet-tooltip leaflet-zoom-animated leaflet-tooltip-bottom")[0].innerText = 'Click to finish' + `\n 宽度:  ${(horizontalDistance/1000).toFixed(2)}  KM` +  `\n 高度:  ${(verticalDistance/1000).toFixed(2)}  KM`
        }
        return 
    }
    componentDidMount() {

        //init map
        this.map = L.map('map', {
            attributionControl: false
        }).setView([40, 116], 10)

        let Gaodesatelayer = L.tileLayer('http://webst0{s}.is.autonavi.com/appmaptile?lang=en&style=6&x={x}&y={y}&z={z}', {
            attribution: '&copy; 高德地图',
            maxZoom: 21,
            minZoom: 3,
            subdomains: "1234"
        });
        let GaodeAnnotionlayer = L.tileLayer('http://wprd0{s}.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=en&size=1&scl=1&style=8', {
            attribution: '&copy; 高德地图',
            maxZoom: 21,
            minZoom: 3,
            subdomains: "1234"
        });

        let GooleVector = L.tileLayer(`https://mp.lizhengtech.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i535257774!3m17!2s${lan == "en" ? 'en-US' : 'zh-CN'}!3s${lan == "en" ? 'US' : 'CN'}!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZjJjNWE3MSxzLmU6bC5pfHAudjpvZmYscy5lOmwudC5mfHAuYzojZmY5N2FlZDMscy5lOmwudC5zfHAuYzojZmYyNDJmM2Uscy50OjE5fHMuZTpsLnQuZnxwLmM6I2ZmOTM5MWExLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjJ8cy5lOmwudC5mfHAuYzojZmY2ZmIwZTIscy50OjQwfHMuZTpnfHAuYzojZmYyNjNjM2Yscy50OjQwfHMuZTpsLnQuZnxwLmM6I2ZmNmI5YTc2LHMudDozfHMuZTpnfHAuYzojZmY1YjY2NzYscy50OjN8cy5lOmcuc3xwLmM6I2ZmMjEyYTM3LHMudDozfHMuZTpsLnQuZnxwLmM6I2ZmOWNhNWIzLHMudDo0OXxzLmU6Z3xwLmM6I2ZmNWQ2Nzk4fHAuczo2fHAubDotM3xwLnY6b24scy50OjQ5fHMuZTpnLnN8cC5jOiNmZjFmMjgzNSxzLnQ6NDl8cy5lOmx8cC52Om9uLHMudDo0OXxzLmU6bC5pfHAudjpvZmYscy50OjQ5fHMuZTpsLnQuZnxwLmM6I2ZmOGViNmY1LHMudDo0fHMuZTpnfHAuYzojZmYyZjM5NDgscy50OjR8cy5lOmwuaXxwLnY6b2ZmLHMudDo2NnxzLmU6bC50LmZ8cC5jOiNmZmQ1OTU2MyxzLnQ6NnxzLmU6Z3xwLmM6I2ZmMTcyNjNjLHMudDo2fHMuZTpsLnQuZnxwLmM6I2ZmNTE1YzZkLHMudDo2fHMuZTpsLnQuc3xwLmM6I2ZmMTcyNjNj!4e0!23i1379896!23i1379903!23i1376099&key=AIzaSyCeCzXVJo5AS-j4-K_tL-FPkMwjruQkzP4`, {
            maxZoom: 21,
            minZoom: 3,
        })

        var googleSateLayer = L.tileLayer(`https://ms.lizhengtech.com/vt?lyrs=s&v=883&hl=${lan == "en" ? 'en-US' : 'zh-CN'}&gl=CN&x={x}&y={y}&z={z}`, {
            maxZoom: 18,
            minZoom: 3,
        })
        var googleRouteLayer = L.tileLayer(`https://mp.lizhengtech.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i535257774!3m17!2s${lan == "en" ? 'en-US' : 'zh-CN'}!3s${lan == "en" ? 'US' : 'CN'}!5e18!12m4!1e68!2m2!1sset!2sRoadmapSatellite!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjJ8cy5lOmx8cC52Om9mZg!4e0!23i1379896!23i1379903!23i1376099&key=AIzaSyCeCzXVJo5AS-j4-K_tL-FPkMwjruQkzP4`, {
            maxZoom: 18,
            minZoom: 3,
        });

        var gaodeSateMap = L.layerGroup([Gaodesatelayer, GaodeAnnotionlayer]);
        var GoogleSateMap = L.layerGroup([googleSateLayer, googleRouteLayer])

        let GoolgeImage = '卫星地图';
        let GoogleMap = '矢量地图';
        let GaodeMap = '高德地图';
        let GaodeRoute = '高德路网'

        let baseMaps = {
            [GoolgeImage]: GoogleSateMap,
            [GoogleMap]: GooleVector,
            [GaodeMap]: Gaodesatelayer,
            [GaodeRoute]: gaodeSateMap
        };

        var options = {
            position: 'topleft', // toolbar position, options are 'topleft', 'topright', 'bottomleft', 'bottomright'
            drawMarker: false, // adds button to draw markers
            drawPolyline: false, // adds button to draw a polyline
            drawRectangle: true, // adds button to draw a rectangle
            drawPolygon: false, // adds button to draw a polygon
            drawCircle: false, // adds button to draw a cricle
            cutPolygon: false, // adds button to cut a hole in a polygon
            dragMode: false,
            drawCircleMarker: false,
            editMode: false, // adds button to toggle edit mode for all layers
            removalMode: false, // adds a button to remove layers
        };
        var lan = "zhcn"

        GoogleSateMap.addTo(this.map)

        // L.tileLayer(`https://https://mp.lizhengtech.com/maps/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i535257774!3m17!2s${lan == "en" ? 'en-US' : 'zh-CN'}!3s${lan == "en" ? 'US' : 'CN'}!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZjJjNWE3MSxzLmU6bC5pfHAudjpvZmYscy5lOmwudC5mfHAuYzojZmY5N2FlZDMscy5lOmwudC5zfHAuYzojZmYyNDJmM2Uscy50OjE5fHMuZTpsLnQuZnxwLmM6I2ZmOTM5MWExLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjJ8cy5lOmwudC5mfHAuYzojZmY2ZmIwZTIscy50OjQwfHMuZTpnfHAuYzojZmYyNjNjM2Yscy50OjQwfHMuZTpsLnQuZnxwLmM6I2ZmNmI5YTc2LHMudDozfHMuZTpnfHAuYzojZmY1YjY2NzYscy50OjN8cy5lOmcuc3xwLmM6I2ZmMjEyYTM3LHMudDozfHMuZTpsLnQuZnxwLmM6I2ZmOWNhNWIzLHMudDo0OXxzLmU6Z3xwLmM6I2ZmNWQ2Nzk4fHAuczo2fHAubDotM3xwLnY6b24scy50OjQ5fHMuZTpnLnN8cC5jOiNmZjFmMjgzNSxzLnQ6NDl8cy5lOmx8cC52Om9uLHMudDo0OXxzLmU6bC5pfHAudjpvZmYscy50OjQ5fHMuZTpsLnQuZnxwLmM6I2ZmOGViNmY1LHMudDo0fHMuZTpnfHAuYzojZmYyZjM5NDgscy50OjR8cy5lOmwuaXxwLnY6b2ZmLHMudDo2NnxzLmU6bC50LmZ8cC5jOiNmZmQ1OTU2MyxzLnQ6NnxzLmU6Z3xwLmM6I2ZmMTcyNjNjLHMudDo2fHMuZTpsLnQuZnxwLmM6I2ZmNTE1YzZkLHMudDo2fHMuZTpsLnQuc3xwLmM6I2ZmMTcyNjNj!4e0!23i1379896!23i1379903!23i1376099&key=AIzaSyCeCzXVJo5AS-j4-K_tL-FPkMwjruQkzP4`, {
        //     maxZoom: 18,
        //     minZoom: 4,
        // }).addTo(this.map);

        this.map.zoomControl.setPosition('bottomright')

        console.log("this.map", this.map, L.zoomControl)

        L.control.layers(baseMaps, null, { position: 'bottomright' }).addTo(this.map);


        this.map.pm.addControls(options)

        this.map.on('pm:create', e => {
            this.layer = e.layer
            this.layerGeojson = this.layer.toGeoJSON
            console.log("layer", this.layer, this.layerGeojson)
            this.map.removeLayer(this.layer)
            const { _northEast, _southWest } = this.layer._bounds
            this.range.maxLat = _southWest.lat.toFixed(6)
            this.range.minLat = _northEast.lat.toFixed(6)
            this.range.minLng = _southWest.lng.toFixed(6)
            this.range.maxLng = _northEast.lng.toFixed(6)
            // console.log("-----------",this.ChildRef.current)
            this.ChildRef.current.showModal()
        })

        // listen to when drawing mode gets disabled
        this.map.on('pm:drawend', (e) => {
            console.log('pm drawn end', e)
            this.map.off("click", this.onListenHandle)
            this.map.off("mousemove", this.caclDistance)
            this.firstPoint = null
        });

        // listen to when a new layer is created
        this.map.on('pm:create', function (e) {
            console.log('pm create', e)
        });

        // listen to vertexes being added to the workingLayer (works only on polylines & polygons)
        this.map.on('pm:drawstart', (e) => {
            this.map.on("click", this.onListenHandle)
            this.map.on("mousemove", this.caclDistance)
        });

    }


    isInvalid = (point) => {
        if (!point) {
            return "输入为空"
        }
        let temp = point.split(",")
        if (temp.length != 2) {
            return "输入有误"
        }
        return temp
    }

    componentDidUpdate() {
        const { point } = this.props
        if (!point) {
            return
        }
        let res = this.isInvalid(point)
        if (typeof (res) == "string") {
            alert(res)
            return
        }
        this.map.setView([parseFloat(res[0]), parseFloat(res[1])], 13)
    }
    render() {
        return (
            <div>
                <div id="map" style={{ height: "100vh", width: "100vw" }} />
                <ModalStyle>
                    <MyModal ref={this.ChildRef} range={this.range} ></MyModal>
                </ModalStyle>
            </div>


        );
    }
}


export default Map;

