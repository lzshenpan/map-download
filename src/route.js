import React from 'react'
import {Routes,Route,Navigate} from 'react-router-dom'
import Sign from './pages/sign'
import Dashboard from './map'

export default function PublicRoute() {

  return (
    <Routes >
        <Route path='/sign' element = {<Sign />} ></Route>
        <Route path='/dashboard'  element={<Dashboard />}></Route>
        <Route path='/*' element={<Navigate to='/sign'> </Navigate>} ></Route>
    </Routes>
  )
}
