import React from 'react';
import ReactDOM from 'react-dom/client';
import PublicRoute from './route';
import { BrowserRouter } from 'react-router-dom';
import 'antd/dist/antd.min.css'
import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <BrowserRouter>
      <PublicRoute />
    </BrowserRouter>
);

