import React, { useContext, useReducer } from 'react';

export const DownloadContext = React.createContext()

const downloadReducer = (state, action) => {
    switch (action.type) {
        case "success":
            return {...state,...action.payload}
        case "fail":
            return {...state,...action.payload}
        default:
            return state
    }
}


const connReducer = (state=false,action)=>{
    switch(action.type){
        case "ok":
            return true
        case "no":
            return false
        default:
            return state
    }
}

const dlPanelReducer = (state=false,action)=>{
    switch(action.type){
        case "show":
            return true
        case "noshow":
            return false
        default:
            return state
    }
}


export const DownloadWrapper = props=>{
    const [downLoadState,dispatch] = useReducer(downloadReducer,{ 
        percent: 0,
        tileDone: 0,
        tileErr: 0,
        finish: false,
        downloading: false})
    const [connState,connDispatch] = useReducer(connReducer,false)
    const [showDlPanel,showDispatch] = useReducer(dlPanelReducer,false)
    return (
        <DownloadContext.Provider value={{downLoadState,dispatch, connState,connDispatch, showDlPanel,showDispatch}}>
            {props.children}
        </DownloadContext.Provider>
    )
}









