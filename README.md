## Map Downloader v1.0

#### Client side Web
#### env develop
node v12.18.3 

#### basic use
``` javascript
    yarn install 
    yarn build 
```

#### 目录结构

### To to list
- [x] 登录
    - [x] 登录界面
    - [x] 路由跳转
    - [x] 密码修改
- [x] 地图任务
    - [x] 地图类型切换
    - [x] 地图下载权限控制
    - [x] 地图定位切换
    - [x] 下载区域选择
- [x] 下载器控制
    - [x] 使用区域定位，判断控制地图下载类型
    - [x] 添加地图
    - [x] 并发协程数目控制
- [x] 用户中心
    - [x] 后台连接检测
    - [x] 历史记录列表实时更新
    - [ ] localstorage优化
- [x] 任务管理
    - [x] 查看下载器状态
    - [x] 当前任务

#### Demo
